import { TestBed, ComponentFixture } from '@angular/core/testing'

import { MedicoComponent } from "./medico.component";
import { MedicoService } from './medico.service';
import { HttpClientModule } from '@angular/common/http';

describe('Medico Component', () => {
  let componente: MedicoComponent;
  let fixture: ComponentFixture<MedicoComponent>;

  //en las pruebas de intergacion indicamos a Angular que debe compilar y trabajar con elementos
  //como el HTML
  beforeEach(() => {
    //testeBed es muy pareceido a un módulo, le indicamos lo que necesitamos,
    //para probar el componente
    TestBed.configureTestingModule({
      declarations: [MedicoComponent],
      //aqui ponemos los servicios
      providers: [MedicoService],
      //aqui definimos los módulos
      //HTTP es para el servicio que tiene el modulo en el constructor
      imports: [HttpClientModule]
    });

    //regresa un ComponentFixture que nos sirve para acceder a todo lo del DOM
    fixture = TestBed.createComponent(MedicoComponent);
    //en componente vamos a tener todo el acceso al componente
    componente = fixture.componentInstance;
  });

  it('Debe de crearse el componente', () => {
    expect(componente).toBeTruthy();
  });

  it('Debe de retornar el nombre del médico', () => {
    const nombre = 'Juan';
    const mensaje = componente.saludarMedico(nombre);
    expect(mensaje).toContain(nombre);
  });

});