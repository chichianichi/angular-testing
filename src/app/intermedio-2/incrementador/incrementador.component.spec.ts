import { TestBed, ComponentFixture } from '@angular/core/testing';
import { IncrementadorComponent } from './incrementador.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';


describe('Incremendator Component', () => {

    let component: IncrementadorComponent;
    let fixture: ComponentFixture<IncrementadorComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [IncrementadorComponent],
            imports: [FormsModule]
        });

        fixture = TestBed.createComponent(IncrementadorComponent);
        component = fixture.componentInstance;

    });

    it('Debe de mostrar la leyenda', () => {
        component.leyenda = 'Progreso de carga';

        //indicamos al componente que hay un cambio que debe renderizar
        //debemos disparar la detección de cambios
        fixture.detectChanges();

        //consultamos un elemento del HTML con query
        const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
        expect(elem.innerHTML).toContain('Progreso de carga');
    });

    it('Debe mostrar en el input el valor del progreso', (done) => {
        component.cambiarValor(5);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const elem: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
            expect(elem.valueAsNumber).toBe(55);
            done();
        });
    });

    it('Debe de incrementar/decrementar en 5 con un click en el botón', () => {
        const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
        botones[0].triggerEventHandler('click', null);
        expect(component.progreso).toBe(45);
        botones[1].triggerEventHandler('click', null);
        expect(component.progreso).toBe(50);
    });

    it('El progreso del componente debe haber cambiado después de presionar el botón', () => {
        // const valorInicial = component.progreso;
        // const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
        // botones[0].triggerEventHandler('click', null);
        // expect(component.progreso).not.toBe(valorInicial);
        const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
        botones[0].triggerEventHandler('click', null);
        fixture.detectChanges();
        const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
        //todos los valores de un HTML son strings
        expect(elem.innerHTML).toContain('45');
    })

});
