import { obtenerRobots } from "./arreglos";

xdescribe('Prueba de arreglos', () => {

  it('Debe de retornar al menos 3 robots', () => {
    const res = obtenerRobots();
    //para verificar >= usamos toBeGraterThanOrEqual
    expect(res.length).toBeGreaterThanOrEqual(3);
  });

  it('Debe de existir Megaman y Ultron', () => {
    const res = obtenerRobots();
    //Revisa si en el arreglo existe un elemento toContain()
    expect(res).toContain('Megaman');
    expect(res).toContain('Ultron');
  });
});