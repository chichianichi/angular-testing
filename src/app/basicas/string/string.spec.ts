// //sirve para agrupar pruebas
// describe('Pruebas de strings');
// //es una prueba
// it('Debe de regresar un string');

import { mensaje } from "./string";

describe('Pruebas de strings', () => {

  it('Debe de regresar un string', () => {
    const respuesta = mensaje('Alexis');

    //verificamos si la prueba es exitosa
    //expect lo usamos para verificar una respuesta
    //toBe es la respuesta idonea para que la prueba pase
    expect(typeof respuesta).toBe('string');
  });

  it('Debe de regresar un saludo con el nombre enviado', () => {
    const nombre = 'Pedro';
    const respuesta = mensaje(nombre);
    //toContain revisa si el string existe en la respuesta
    expect(respuesta).toContain(nombre);
  })
});