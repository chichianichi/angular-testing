import { usuarioLogeado } from "./booleanos";

describe('Pruebas de booleanos', () => {

  it('Debe de retornar true', () => {
    const res = usuarioLogeado();
    //toBeTruthy verifica que la respuesta sea true
    expect(res).toBeTruthy();
  });
});