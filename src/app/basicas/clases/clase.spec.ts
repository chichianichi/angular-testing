import { Jugador } from "./clase";

//para revisar el reporte de cobertura de código usamos la siguiente línea:
// ng test --code-coverage

describe('Pruebas de clase', () => {
  let jugador = new Jugador();

  //ciclo de vida de las pruebas
  beforeAll(() => {
    // console.log('beforeAll');
  });
  beforeEach(() => {
    // console.log('beforeEach');
    //Como en cada prueba se va a  alterar el hp
    //nosotros debemos reiniciar ese atributo antes de cada prueba
    // jugador.hp = 100;
    jugador = new Jugador();
  });
  afterAll(() => {
    // console.log('afterAll');
  });
  afterEach(() => {
    // console.log('afterEach');
  });


  it('Debe retornar 80 de hp si recibe 20 de daño', () => {
    // const jugador = new Jugador();
    const res = jugador.recibeDanio(20);

    expect(res).toBe(80);
  });

  it('Debe retornar 50 de hp si recibe 50 de daño', () => {
    // const jugador = new Jugador();
    const res = jugador.recibeDanio(50);

    expect(res).toBe(50);
  });

  it('Debe retornar 0 de hp si recibe más de 100 de daño', () => {
    // const jugador = new Jugador();
    const res = jugador.recibeDanio(120);

    expect(res).toBe(0);
  });
});
