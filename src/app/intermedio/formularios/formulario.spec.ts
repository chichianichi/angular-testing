import { FormBuilder } from "@angular/forms";
import { FormularioRegister } from "./formulario";

describe('Formularios', () => {
  let componente: FormularioRegister;

  //Hay componentes que requieren iniciializar por constructor
  beforeEach(() => {
    componente = new FormularioRegister(new FormBuilder);
  });

  it('Debe de crear un formulario con dos campos: email y password', () => {
    //con form.contains verificamos que el formulario tenga un campo que le indicamos
    expect(componente.form.contains('email')).toBeTruthy();
    expect(componente.form.contains('password')).toBeTruthy();
  });

  it('El email debe de ser obligatorio', () => {
    const control = componente.form.get('email');
    control?.setValue('');
    expect(control?.valid).toBeFalsy();
  });

  it('El email debe de ser un correo válido', () => {
    const control = componente.form.get('email');
    control?.setValue('alexis@gmail.com');
    expect(control?.valid).toBeTruthy();
  });
});