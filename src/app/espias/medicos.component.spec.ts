import { Observable, from, throwError } from 'rxjs';
import { MedicosComponent } from './medicos.component';
import { MedicosService } from './medicos.service';


describe('MedicosComponent', () => {
    //para componentes podemos pobarlos como clases
    let componente: MedicosComponent;
    const servicio = new MedicosService(null as any);

    beforeEach(() => {
        componente = new MedicosComponent(servicio);
    });


    it('Init debe de cargar los médicos', () => {
        const medicos = ['medico 1', 'medico 2', 'medico 3'];
        //el spy nos permite hacer peticiones falsas y fingir el comportamiento
        //esto habilita el "servicio" para que cuando llamemos la funcion del componente.ts
        //reciba lo que le indica el espia
        spyOn(servicio, 'getMedicos').and.callFake(() => {
            return from([medicos]);
        });
        //para el onInit lo llamamos manualmente
        componente.ngOnInit();
        expect(componente.medicos.length).toBeGreaterThan(0);
    });

    it('Debe de llamar al servidor para agregar un medico', () => {

        const espia = spyOn(servicio, 'agregarMedico').and.callFake(() => {
            return new Observable();
        });

        componente.agregarMedico();

        //revisa la simulacion de una llamada
        expect(espia).toHaveBeenCalled();
    });

    it('Debe de agregar un nuevo médico al arreglo de médicos', () => {
        const medico = { id: 1, nombre: 'Juan' };

        spyOn(servicio, 'agregarMedico').and.returnValue(from([medico]));
        componente.agregarMedico();

        expect(componente.medicos.indexOf(medico)).toBeGreaterThanOrEqual(0);
    });

    it('Si falla la adición la propiedad mensajeError, debe ser igual a error del servicio', () => {
        const miError = 'No se pudo agegar el médico';

        spyOn(servicio, 'agregarMedico').and
            .returnValue(throwError(miError));

        componente.agregarMedico();

        expect(componente.mensajeError).toBe(miError);
    });

    it('Debe de llamar al servidor para borrar un médico', () => {
        //la función del servicio espera una confirmación antes de llamar al servicio
        //esto lo simulamos con un espia sbore window
        //esto automatiza el valor que vaya a recibir para la prueba
        spyOn(window, 'confirm').and.returnValue(true);
        const espia = spyOn(servicio, 'borrarMedico').and.returnValue(new Observable());
        componente.borrarMedico('1');
        //toHaveBeenCalledWith revisa el parámetro que enviamos a la llamada del servicio
        expect(espia).toHaveBeenCalledWith('1');
    });

    it('No debe de llamar al servidor para borrar un médico', () => {
        spyOn(window, 'confirm').and.returnValue(false);
        const espia = spyOn(servicio, 'borrarMedico').and.returnValue(new Observable());
        componente.borrarMedico('1');
        expect(espia).not.toHaveBeenCalledWith('1');
    });
});
