import { MedicoComponent } from "src/app/intermedio-2/medico/medico.component";
import { RUTAS } from "./app.routes";

describe('Rutas principales', () => {

  it('Debe existir la ruta /medico/:id', () => {
    //toContain puede servir para revisar si existe un objeto en un arreglo
    expect(RUTAS).toContain(
      { path: 'medico/:id', component: MedicoComponent }
    );
  });
})
